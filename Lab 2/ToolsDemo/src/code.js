/**
 * Created by b00241075 on 20/02/2015.
 */

function Rect(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

Rect.prototype.move = function(dx, dy){
    this.x+=dx;
    this.y+=dy;
}

Rect.prototype.contains = function(x, y){
    if(x <= this.x + this.width && x >= this.x){
        if(y <= this.y + this.height && y >= this.y){
            return true;
        }
    }

    return false;
}

Rect.prototype.overlap = function(other){
    if (this.x <= other.x + other.width &&
        this.x + this.width >= other.x &&
        this.y <= other.y + other.height &&
        this.height + this.y >= other.y){
        return true;
    }

    return false;
}

Rect.prototype.is = function(other){
    return this === other;
}

Rect.prototype.equals = function(other){
    if(this.x === other.x && this.y === other.y && this.width === other.width && this.height === other.height){
        return true;
    }
    return false;
}