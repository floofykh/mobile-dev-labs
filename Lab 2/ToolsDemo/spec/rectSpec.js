/**
 * Created by b00241075 on 20/02/2015.
 */
describe("Tests the Rect type", function(){
    var r;

    beforeEach(function(){
        r = new Rect(10, 20, 40, 30);
    })

    it("creates a new Rect object and assigns values for position and size", function(){
        expect(r).toBeDefined();
        expect(r.x).toEqual(10.0);
        expect(r.y).toEqual(20.0);
        expect(r.width).toEqual(40.0);
        expect(r.height).toEqual(30.0);
    });

    it("tests that a Rect object can be moved to a new location", function(){
        r.move(50, 30);
        expect(r.x).toEqual(60);
        expect(r.y).toEqual(50);
    });

    it("tests whether a point is inside the Rect", function(){
        expect(r.contains(0, 0)).toEqual(false);
        expect(r.contains(100, 100)).toEqual(false);
        expect(r.contains(10, 20)).toEqual(true);
        expect(r.contains(30, 35)).toEqual(true);
        expect(r.contains(51, 25)).toEqual(false);
    })

    it("tests whether rectangles overlap", function(){
        var r2 = new Rect(10, 20, 40, 30);//Same Rect
        var r3 = new Rect(15, 25, 30, 20);//Inside
        var r4 = new Rect(40, 40, 40, 30);//Overlap
        var r5 = new Rect(50, 20, 40, 30);//On edge
        var r6 = new Rect(10, 51, 40, 30);//Outside

        expect(r.overlap(r2)).toBe(true);
        expect(r.overlap(r3)).toBe(true);
        expect(r.overlap(r4)).toBe(true);
        expect(r.overlap(r5)).toBe(true);
        expect(r.overlap(r6)).toBe(false);
        expect(r2.overlap(r)).toBe(true);
        expect(r3.overlap(r)).toBe(true);
        expect(r4.overlap(r)).toBe(true);
        expect(r5.overlap(r)).toBe(true);
        expect(r6.overlap(r)).toBe(false);
    })

    it("tests whether two rectangles are the same instance", function(){
        var r2 = r;

        expect(r.is(r2)).toBe(true);
        expect(r2.is(r)).toBe(true);
    })

    it("tests whether two rectangles are equal", function(){
        var r2 = new Rect(10, 20, 40, 30);//Same Rect
        var r3 = new Rect(15, 25, 30, 20);//Inside

        expect(r.equals(r2)).toBe(true);
        expect(r.equals(r3)).toBe(false);
        expect(r2.equals(r)).toBe(true);
        expect(r3.equals(r)).toBe(false);
    })
});