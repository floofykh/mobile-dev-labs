
var canvas, //HTML5 canvas element
    context, //2D graphics context
    frameID,
    tileSet,
    character,
    row, //counter for row number
    col, //column number
    TILE_SIZE = 16,
    NUM_ROWS = 30,
    NUM_COLS = 40,
    NUM_ASSETS = 2,
    assetsLoaded = 0;

function gameLoop()
{
    if(assetsLoaded == NUM_ASSETS){
        drawTiles(context, tileSet);
        character.draw(context);
    }
    frameID = requestAnimationFrame(gameLoop);
}

function drawTiles(ctx, tileSet)
{
    var destx, desty, srcx;
    for(row=0; row < NUM_ROWS; row+=1)
    {
        for(col = 0; col < NUM_COLS; col+=1)
        {
            destx = col*TILE_SIZE,
            desty = row*TILE_SIZE;
            srcx = tile_map[row][col] * TILE_SIZE;
            tileSet.draw(context, tile_map[row][col], 0, destx, desty, TILE_SIZE, TILE_SIZE);
        }
    }
}

function loadTiles(src)
{
    tileSet = new TileSet(1, 4, TILE_SIZE, TILE_SIZE);
    tileSet.load(src, function()
    {
        assetsLoaded++;
    });
}

function loadCharacter()
{
    var characterSprite = new TileSet(4, 9, 16, 18);
    characterSprite.load("images/sara 16x18 source.png", function(){
        assetsLoaded++;
    })

    character = new Character(0, 26, characterSprite);
}

window.onload = function()
{
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    loadTiles("images/tileset.png");
    loadCharacter();
    gameLoop();
}

function TileSet(numRows, numCols, tileSizeX, tileSizeY)
{
    this.image = null;
    this.numRows = numRows;
    this.numCols = numCols;
    this.tileSizeX = tileSizeX;
    this.tileSizeY = tileSizeY;

    this.load = function(file, onload)
    {
        this.image = new Image();
        this.image.src = file;
        this.image.onload = function() {
            onload();
        }
    }

    this.draw = function(context, col, row, destX, destY, sizeX, sizeY)
    {
        if(col < numCols && row < numRows)
            context.drawImage(this.image, tileSizeX*col, tileSizeY*row, tileSizeX, tileSizeY, destX, destY, sizeX, sizeY);
    }
}

function Character(row, col, tileSet)
{
    this.row = row;
    this.col = col;
    this.tileSet = tileSet;
    this.anim = 0;

    this.move = function (x, y) {
        if(this.row+y >= 0 && this.row+y < NUM_ROWS && tile_map[this.row+y][this.col+x] == 0){
            this.row += y;

            if (y < 0)
                this.anim = 0;
            else if (y > 0)
                this.anim = 2;
        }

        if(this.col+x >= 0 && this.col+x < NUM_COLS && tile_map[this.row+y][this.col+x] == 0){
            this.col += x;

            if (x < 0)
                this.anim = 3;
            else if (x > 0)
                this.anim = 1;
        }
    }

    this.draw = function(context)
    {
        this.tileSet.draw(context, 0, this.anim, this.col * TILE_SIZE, this.row * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }
}

document.addEventListener('keydown', function (event) {
    switch (event.keyCode) {
        case 37:
            character.move(-1, 0);
            break;
        case 39:
            character.move(1, 0);
            break;
        case 38:
            character.move(0, -1);
            break;
        case 40:
            character.move(0, 1);
    }
});

